from django.apps import AppConfig


class HandballconnectConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'handballconnect'
